package de.wsc.cli.test;

import de.wsc.cli.CliConfig;
import de.wsc.cli.CliConfiguration;
import de.wsc.cli.Flag;

import java.util.Collection;
import java.util.List;

@CliConfiguration("my.")
public class MyConfiguration {

    private final CliConfig config;

    public MyConfiguration(CliConfig config) {
        this.config = config;
    }
    @Flag(name="value", description = "Value für MyConfiguration")
    private String sValue;

    @Flag(description = "Greeting")
    private String greeting;

    private Integer iValue;
    private Boolean bValue;
    private List<String> sList;
    private Collection<String> s1List;
    private List<Integer> iList;

    public String getSValue() {
        return sValue;
    }

    public void setSValue(String sValue) {
        this.sValue = sValue;
    }

    public Integer getIValue() {
        return iValue;
    }

    public void setIValue(Integer iValue) {
        this.iValue = iValue;
    }

    public Boolean getBValue() {
        return bValue;
    }

    public void setBValue(Boolean bValue) {
        this.bValue = bValue;
    }

    public List<String> getSList() {
        return sList;
    }

    public void setSList(List<String> sList) {
        this.sList = sList;
    }

    public List<Integer> getIList() {
        return iList;
    }

    public void setIList(List<Integer> iList) {
        this.iList = iList;
    }

    public Collection<String> getS1List() {
        return s1List;
    }

    public void setS1List(Collection<String> s1List) {
        this.s1List = s1List;
    }

    public CliConfig getConfig() {
        return config;
    }

    private void setHello(String hello) {
        System.out.println(hello);
    }
}
