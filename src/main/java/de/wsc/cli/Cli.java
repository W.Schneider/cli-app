package de.wsc.cli;

import java.lang.reflect.*;
import java.util.*;

public class Cli {

    private static class CliParameter {
        String name;
        String description;
        boolean mandatory;
        boolean missingValue;

        public CliParameter(String name, String description, boolean mandatory, boolean missingValue) {
            this.name = name;
            this.description = description;
            this.mandatory = mandatory;
            this.missingValue = missingValue;
        }
    }

    private static class CliFlag extends CliParameter implements Comparable<CliFlag> {
        public CliFlag(String name, String description, boolean mandatory, boolean missingValue) {
            super(name, description, mandatory, missingValue);
        }

        @Override
        public int compareTo(CliFlag other) {
            if (other == null) return 1;
            return name.compareTo(other.name);
        }
    }

    private static class CliArgument extends CliParameter implements Comparable<CliArgument> {
        final int index;

        public CliArgument(int index, String name, String description, boolean mandatory, boolean missingValue) {
            super(name, description, mandatory, missingValue);
            this.index = index;
        }

        @Override
        public int compareTo(CliArgument other) {
            if (other == null) return 1;
            return index - other.index;
        }
    }

    private static class CliErrorCode {
        int code;
        String description;

        public CliErrorCode(int code, String description) {
            this.code = code;
            this.description = description;
        }
    }

    private static final Map<Object, Object> classInstances = new HashMap<>();
    private static final TreeSet<CliArgument> cliArguments = new TreeSet<>();
    private static final TreeSet<CliFlag> cliFlags = new TreeSet<>();
    private static final List<CliErrorCode> cliErrorCodes = new ArrayList<>();

    public static <T extends CliApp> void run(Class<T> cliMainClass, String[] args) {
        try {
            CliConfig config = new CliConfig();
            classInstances.put(CliConfig.class, config);
            List<String> arguments = config.init(args);
            T tApp = createInstance(cliMainClass);
            processAnnotations(tApp, config);

            if (config.containsKey("help")) {
                printUsage(cliMainClass);
                return;
            }

            String mandatory = checkMandatory();
            if (mandatory != null) {
                System.out.println(mandatory);
                printUsage(cliMainClass);
                System.exit(8);
            }

            tApp.cliMain(arguments, config);
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException |
                 NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
    private static <T> T createInstance(Class<T> tClass)
            throws InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        Constructor<?>[] constructors = tClass.getDeclaredConstructors();
        if (constructors.length > 1) {
            throw new RuntimeException(tClass + " has more than one constructor");
        }

        Parameter[] parameter = constructors[0].getParameters();
        Class<?>[] parameterTypes = constructors[0].getParameterTypes();
        Object[] parameters = new Object[parameterTypes.length];
        for (int i = 0; i < parameter.length; i++) {
            Class<?> pClass = parameter[i].getType();
            if (classInstances.containsKey(pClass)) {
                parameters[i] = classInstances.get(pClass);
            } else if (isCliService(pClass) || isCliConfiguration(pClass)) {
                parameters[i] = createInstance(pClass);
                processAnnotations(parameters[i], (CliConfig) classInstances.get(CliConfig.class));
                classInstances.put(pClass, parameters[i]);
            } else {
                throw new RuntimeException(pClass + " is no CliService");
            }
        }

        Constructor<T> constructor = tClass.getDeclaredConstructor(parameterTypes);
        return constructor.newInstance(parameters);
    }

    private static boolean isCliService(Class<?> pClass) {
        return pClass.isAnnotationPresent(CliService.class);
    }

    private static boolean isCliConfiguration(Class<?> pClass) {
        return pClass.isAnnotationPresent(CliConfiguration.class);
    }

    private static void processAnnotations(Object appOrService, CliConfig config) {
        for (Field field : appOrService.getClass().getDeclaredFields()) {
            processFlag(appOrService, config, field);
            processArgument(appOrService, config, field);
            processErrorCode(appOrService, field);
        }
    }

    private static void processFlag(Object tApp, CliConfig config, Field field) {
        String prefix = "";
        CliConfiguration configAnnotation = tApp.getClass().getAnnotation(CliConfiguration.class);
        if (configAnnotation != null) {
            prefix = configAnnotation.value();
        }
        Flag flag = field.getAnnotation(Flag.class);
        if (flag != null) {
            String key = flag.name();
            if (key.isEmpty()) {
                key = field.getName();
            }
            key = prefix + key;
            String value = config.getProperty(key);
            if ((field.getType() == boolean.class || field.getType() == Boolean.class) && value == null) {
                value = config.containsKey(key) ? "true" : "false";
            }
            setFieldValue(tApp, field, value);

            cliFlags.add(new CliFlag(key, flag.description(), flag.mandatory(), value == null));
        }
    }

    private static void processArgument(Object tApp, CliConfig config, Field field) {
        Argument argument = field.getAnnotation(Argument.class);
        if (argument != null) {
            String key = argument.name();
            if (key.isEmpty()) {
                key = field.getName();
            }
            int index = argument.index() - 1;
            List<String> remainingArguments = config.getRemainingArguments();
            if (index >= 0) {
                if (remainingArguments.size() > index) {
                    if (isStringArray(field) || isStringList(field)) {
                        int size = remainingArguments.size();
                        setFieldValue(tApp, field, remainingArguments.subList(index, size).toArray(new String[size - index]));
                    } else {
                        setFieldValue(tApp, field, remainingArguments.get(index));
                    }
                }
                cliArguments.add(new CliArgument(index, key, argument.description(), argument.mandatory(),
                        remainingArguments.size() <= index));
            }
        }
    }

    private static void processErrorCode(Object tApp, Field field) {
        try {
            ErrorCode errorCode = field.getAnnotation(ErrorCode.class);
            if (errorCode != null) {
                field.setAccessible(true);
                int code = field.getInt(tApp);
                String description = errorCode.description();
                cliErrorCodes.add(new CliErrorCode(code, description));
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private static void setFieldValue(Object appOrService, Field field, String... values) {
        try {
            if (values[0] == null) {
                return;
            }
            Class<?> fieldType = field.getType();

            field.setAccessible(true);

            if (fieldType == String.class) {
                field.set(appOrService, values[0]);
            }
            if (fieldType == int.class || fieldType == Integer.class) {
                field.set(appOrService, Integer.parseInt(values[0]));
            }
            if (fieldType == boolean.class || fieldType == Boolean.class) {
                field.set(appOrService, Boolean.valueOf(values[0]));
            }

            if (isStringList(field)) {
                field.set(appOrService, Arrays.asList(values));
            }
            if (isStringArray(field)) {
                field.set(appOrService, values);
            }
        } catch (IllegalAccessException e) {
            // ignore
        }
    }

    private static boolean isStringList(Field field) {
        Type fieldGenericType = field.getGenericType();
        ParameterizedType parameterizedType = null;
        if (fieldGenericType instanceof ParameterizedType) {
            parameterizedType = (ParameterizedType) fieldGenericType;
        }
        if (parameterizedType != null && parameterizedType.getRawType() == List.class) {
            return parameterizedType.getActualTypeArguments().length == 1 && parameterizedType.getActualTypeArguments()[0] == String.class;
        }
        return false;
    }

    private static boolean isStringArray(Field field) {
        Class<?> fieldType = field.getType();
        return fieldType.isArray() && fieldType.getComponentType() == String.class;
    }

    private static <T extends CliApp> void printUsage(Class<T> cliApp) {
        StringBuilder description = new StringBuilder("\nUsage: ");
        description.append("java ").append(cliApp.getName()).append(' ');

        for (CliFlag cliFlag : cliFlags) {
            description.append(toString(cliFlag)).append(' ');
        }

        for (CliArgument cliArgument : cliArguments) {
            description.append(toString(cliArgument)).append(' ');
        }

        description.append("\n\n");

        App app = cliApp.getAnnotation(App.class);
        if (app != null) {
            description.append(app.description()).append("\n\n");
        }

        for (CliArgument cliArgument : cliArguments) {
            description.append(toString(cliArgument)).append(": ")
                    .append(cliArgument.description).append(cliArgument.mandatory ? " (MANDATORY)" : "")
                    .append('\n');
        }

        description.append("\n");
        for (CliFlag cliFlag : cliFlags) {
            description.append("--").append(cliFlag.name).append(": ")
                    .append(cliFlag.description).append(cliFlag.mandatory ? " (MANDATORY)" : "")
                    .append('\n');
        }

        description.append("\nERROR CODES:\n");

        for (CliErrorCode cliErrorCode : cliErrorCodes) {
            description.append(cliErrorCode.code).append(": ")
                    .append(cliErrorCode.description)
                    .append('\n');
        }

        System.out.println(description);
    }

    private static String toString(CliParameter parameter) {
        String prefix = parameter instanceof CliFlag ? "--" : "";
        if (parameter.mandatory) {
            return '<' + prefix + parameter.name + ">";
        } else {
            return '[' + prefix + parameter.name + "]";
        }
    }

    private static String checkMandatory() {
        StringBuilder result = new StringBuilder();
        for (CliFlag cliFlag : cliFlags) {
            if (cliFlag.mandatory && cliFlag.missingValue) {
                result.append("--").append(cliFlag.name).append(' ');
            }
        }
        for (CliArgument cliArgument : cliArguments) {
            if (cliArgument.mandatory && cliArgument.missingValue) {
                result.append('<').append(cliArgument.name).append('>').append(' ');
            }
        }
        if (!result.isEmpty()) {
            return "Missing mandatory parameter(s):\n" + result + '\n';
        } else {
            return null;
        }
    }
}
