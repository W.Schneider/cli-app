package de.wsc.cli.test;

import de.wsc.cli.Argument;
import de.wsc.cli.CliService;

@CliService
public class MyService1 {

    @Argument(index = 0)
    private String filename;

    private final MyService service;

    public MyService1(MyService service) {
        this.service = service;
    }

    public void print() {
        System.out.println("service: " + service);
    }
}
