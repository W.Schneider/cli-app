package de.wsc.cli;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CliConfigTest {

    @Test
    void getSeparator() {
        CliConfig config = new CliConfig();
        config.init(new String[] {"--cliConfig:none", "--m.list:eins,zwei"});
        assertEquals(",", config.getSeparator("m.list"));

        config = new CliConfig();
        config.init(new String[] {"--cliConfig:none", "--m.list:eins;zwei", "--m.list.separator:;"});
        assertEquals(";", config.getSeparator("m.list"));

        config = new CliConfig();
        config.init(new String[] {"--cliConfig:none", "--m.list:eins.zwei", "--m.separator:."});
        assertEquals(".", config.getSeparator("m.list"));

        config = new CliConfig();
        config.init(new String[] {"--cliConfig:none", "--m.list:eins#zwei", "--separator:#"});
        assertEquals("#", config.getSeparator("m.list"));
    }

}