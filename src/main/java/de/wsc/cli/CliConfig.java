package de.wsc.cli;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class CliConfig {
    private final Map<String, String> appProperties = new HashMap<>();
    private final Properties fileProperties = new Properties();
    private List<String> remainingArguments = Collections.emptyList();

    public List<String> init(String[] args) {
        remainingArguments = processArguments(args);
        String filename = getProperty("cliConfig", "cli.properties");
        readFileArguments(filename);
        return remainingArguments;
    }

    private List<String> processArguments(String[] args) {
        List<String> remainingArguments = new ArrayList<>();
        for (String arg : args) {
            if (isConfigArgument(arg)) {
                appProperties.put(getKey(arg), getValue(arg));
            } else {
                remainingArguments.add(arg);
            }
        }
        return remainingArguments;
    }

    private String getKey(String arg) {
        int posOfColon = arg.indexOf(":");
        if (posOfColon < 0) {
            return arg.substring("--".length());
        } else {
            return arg.substring("--".length(), posOfColon);
        }
    }

    private String getValue(String arg) {
        int posOfColon = arg.indexOf(":");
        if (posOfColon < 0) {
            return null;
        } else {
            return arg.substring(posOfColon + 1);
        }
    }

    private boolean isConfigArgument(String arg) {
        return arg != null && arg.startsWith("--");
    }

    private void readFileArguments(String filename) {
        try {
            if (Files.exists(Path.of(filename))) {
                fileProperties.load(new FileReader(filename));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getProperty(String key) {
        return getProperty(key, null);
    }

    public String getProperty(String key, String defaultValue) {
        String value;
        value = appProperties.get(key);
        if (value == null) {
            value = System.getProperty(key);
        }
        if (value == null) {
            value = System.getenv(key);
        }
        if (value == null) {
            value = fileProperties.getProperty(key);
        }
        if (value == null) {
            value = defaultValue;
        }
        return value;
    }

    public boolean containsKey(String key) {
        boolean contains;
        contains = appProperties.containsKey(key);
        if (!contains) {
            contains = System.getProperties().contains(key);
        }
        if (!contains) {
            contains = System.getenv().containsKey(key);
        }
        if (!contains) {
            contains = fileProperties.containsKey(key);
        }
        return contains;
    }

    public Boolean getPropertyAsBoolean(String key) {
        String value = getProperty(key);
        if (value == null) {
            return null;
        } else {
            return Boolean.valueOf(value);
        }
    }

    public Integer getPropertyAsInteger(String key) {
        String value = getProperty(key);
        if (value == null) {
            return null;
        } else {
            return Integer.valueOf(value);
        }
    }

    public Long getPropertyAsLong(String key) {
        String value = getProperty(key);
        if (value == null) {
            return null;
        } else {
            return Long.valueOf(value);
        }
    }

    public List<String> getPropertyAsStringList(String key) {
        String separator = getProperty(getSeparator(key));
        String listValue = getProperty(key);
        if (listValue == null) {
            return new ArrayList<>();
        } else {
            String[] values = listValue.split(separator);
            return new ArrayList<>(Arrays.asList(values));
        }
    }

    String getSeparator(String key) {
        String separatorKey = key;
        String separator;
        do {
            separator = getProperty(separatorKey + ".separator");
            if (separator == null) {
                int lastIndex =  separatorKey.lastIndexOf('.');
                if (lastIndex < 0) {
                    separatorKey = "";
                } else {
                    separatorKey = separatorKey.substring(0, lastIndex);
                }
            }
        } while (!separatorKey.isEmpty() && separator == null);
        return separator == null ? getProperty("separator",",") : separator;
    }

    public List<String> getRemainingArguments() {
        return remainingArguments;
    }
}
