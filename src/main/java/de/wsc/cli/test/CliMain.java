package de.wsc.cli.test;

import de.wsc.cli.*;

import java.util.List;

@App(description = "Dieses Programm macht gar nichts")
public class CliMain extends CliApp {

    @ErrorCode(description="at least one mandatory parameter is missing")
    private static final int MANDATORY_ARGUMENT_MISSED = 8;

    private final MyService service;
    private final MyService1 service1;
    private final MyConfiguration configuration;

    @Argument(index = 2, description = "filename")
    private String filename;

    @Flag(name = "algorithm", description = "AES,DES,...")
    private String hello;


    @Flag(description = "maxmimal count")
    private int count;

    @Flag(description = "force update", mandatory = true)
    private boolean force;

    @Argument(index = 1, description = "hurz", mandatory = true)
    private String hurz;

    @Argument(index = 3, name="files...", description = "files to process", mandatory = true)
    private List<String> files;

    @Argument(index = 3, name="files...", description = "files to process", mandatory = true)
    private String[] files1;

    public CliMain(MyService service, MyService1 service1, MyConfiguration configuration) {
        this.service = service;
        this.service1 = service1;
        this.configuration = configuration;
    }

    public static void main(String[] args) {
        Cli.run(CliMain.class, args);
    }

    @Override
    public void cliMain(List<String> args, CliConfig config) {
        System.out.println("CliApp started, args: " + args);
        System.out.println("config: " + config);
        service.print();
        System.out.println("service: " + service);
        service1.print();
        System.out.println(configuration.getS1List());
        System.out.println(configuration.getConfig().getProperty("hello"));
    }
}
