package de.wsc.cli;

import java.util.List;

public abstract class CliApp {
    public abstract void cliMain(List<String> args, CliConfig config);
}
