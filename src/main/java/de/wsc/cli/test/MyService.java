package de.wsc.cli.test;

import de.wsc.cli.Argument;
import de.wsc.cli.CliConfig;
import de.wsc.cli.CliService;
import de.wsc.cli.Flag;

@CliService
public class MyService {

    @Argument(index = 0, description = "Dies ist der Z-Algorithmus")
    private String zalgorithm;

    private final CliConfig config;

    public MyService(CliConfig config) {
        this.config = config;
    }

    public void print() {
        System.out.println("Heureka Config: " + config);
    }
}
